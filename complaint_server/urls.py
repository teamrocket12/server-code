"""complaint_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from main import views

urlpatterns = [
    url(r'^users/$', views.users_list),
    url(r'^users/authority/$', views.authority_list),
    url(r'^users/(?P<username>\w+)/complaints/all/$', views.user_complaint_list),
    url(r'^users/(?P<username>\w+)/complaints/$', views.user_pending_complaint_list),
    url(r'^users/(?P<username>\w+)/notifications/$', views.notification_list),
    url(r'^users/(?P<username>\w+)/authority/complaints/$', views.authority_pending_complaint_list),
    url(r'^users/(?P<username>\w+)/authority/complaints/all/$', views.authority_complaint_list),
    url(r'^users/(?P<username>\w+)/$', views.user_detail),
    url(r'^users/id/(?P<pk>[0-9]+)/$', views.user_detail_pk),
    url(r'^complaints/$', views.complaints_list),
    url(r'^complaints/(?P<pk>[0-9]+)/$', views.complaint_detail),
    url(r'^complaints/(?P<pk>[0-9]+)/comments/$', views.complaint_comments),
    url(r'^complaints/(?P<pk>[0-9]+)/comments/(?P<pkc>[0-9]+)/$', views.complaint_comment_id),
    url(r'^complaints/(?P<pk>[0-9]+)/authority/$', views.authority_detail),
    # url(r'^/$', views.snippet_list),
    # url(r'^snippets/(?P<pk>[0-9]+)/$', views.snippet_detail),
]
