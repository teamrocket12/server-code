from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    designation = models.CharField(default='Student',max_length=100)
    entry_number = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=100)
    user_type = models.IntegerField(default=0)


class Complaints(models.Model):
	user=models.ForeignKey(UserProfile,on_delete=models.CASCADE)
	c_type=models.IntegerField()
	c_subtype=models.IntegerField()
	title=models.CharField(default="",max_length=100)
	description=models.TextField(default="")
	anonymous=models.BooleanField(default=False)
	visiblity_level=models.IntegerField(default=0)
	urgent=models.BooleanField(default=False)
	date_time_submit=models.DateTimeField(null=True)
	date_time_resolve=models.DateTimeField(null=True)
	no_of_upvotes=models.IntegerField(default=0)
	status=models.IntegerField(default=1)
	official_reply=models.TextField(default="")


class Authority(models.Model):
	complaint=models.ForeignKey(Complaints,on_delete=models.CASCADE)
	authority=models.ForeignKey(UserProfile,on_delete=models.CASCADE)

class Comments(models.Model):
	complaint=models.ForeignKey(Complaints,on_delete=models.CASCADE)
	user=models.ForeignKey(UserProfile,on_delete=models.CASCADE)
	text=models.TextField(default="")
	date_time=models.DateTimeField(null=True)

class Upvotes(models.Model):
	complaint=models.ForeignKey(Complaints,on_delete=models.CASCADE)
	user=models.ForeignKey(UserProfile,on_delete=models.CASCADE)

class Notifications(models.Model):
	complaint=models.ForeignKey(Complaints,on_delete=models.CASCADE)
	user=models.ForeignKey(UserProfile,on_delete=models.CASCADE)
	n_type=models.IntegerField(default=0)
	status=models.BooleanField(default=False)
	text=models.TextField(default="")
	date_time=models.DateTimeField(null=True)
	date_time_seen=models.DateTimeField(null=True)

# Create your models here.
