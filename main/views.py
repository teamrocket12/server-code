from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from main.models import *
from main.serializers import *
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import datetime

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@api_view(['GET', 'POST'])
def users_list(request):
    """
    List all tasks, or create a new task.
    """
    if request.method == 'GET':
        users = UserProfile.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = UserSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET'])
def authority_list(request):
    """
    List all tasks, or create a new task.
    """
    if request.method == 'GET':
    	users = UserProfile.objects.filter(user_type__in=(2,3))        
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)




@api_view(['GET', 'POST'])
def complaints_list(request):
    """
    List all tasks, or create a new task.
    """
    if request.method == 'GET':
        complaints = Complaints.objects.filter(visiblity_level=1)
        serializer = ComplaintSerializer(complaints, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def user_complaint_list(request, username):
    """
    List all tasks, or create a new task.
    """

    try:
        user = UserProfile.objects.get(user=User.objects.get(username=username))
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
        complaints = Complaints.objects.filter(user=user)
        serializer = ComplaintSerializer(complaints, many=True)
        return Response(serializer.data)



@api_view(['GET'])
def authority_complaint_list(request, username):
    """
    List all tasks, or create a new task.
    """

    try:
        user = UserProfile.objects.get(user=User.objects.get(username=username))
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
    	if(user.user_type>1):
	        cids = Authority.objects.filter(authority=user).values('complaint');
	        complaints=Complaints.objects.filter(pk__in=cids)
	        serializer = ComplaintSerializer(complaints, many=True)
	        return Response(serializer.data)



@api_view(['GET'])
def authority_pending_complaint_list(request, username):
    """
    List all tasks, or create a new task.
    """

    try:
        user = UserProfile.objects.get(user=User.objects.get(username=username))
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
    	if(user.user_type>1):
	        cids = Authority.objects.filter(authority=user).values('complaint');
	        complaints=Complaints.objects.filter(pk__in=cids,status=False)
	        serializer = ComplaintSerializer(complaints, many=True)
	        return Response(serializer.data)




@api_view(['GET','PUT'])
def user_pending_complaint_list(request, username):
    """
    List all tasks, or create a new task.
    """

    try:
        user = UserProfile.objects.get(user=User.objects.get(username=username))
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
        complaints = Complaints.objects.filter(user=user,status=False)
        serializer = ComplaintSerializer(complaints, many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ComplaintSerializerInput(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET','DELETE','POST'])
def complaint_detail(request, pk):
    """
    List all tasks, or create a new task.
    """

    try:
        c=Complaints.objects.get(pk=pk)
    except c.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
        serializer = ComplaintSerializer(c)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        c.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    elif request.method == 'POST':
    	serializer=ComplaintSerializer(c,request.data,partial=True)
    	if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    



@api_view(['GET', 'PUT', 'DELETE'])
def user_detail(request, username):
    """
    Get, udpate, or delete a specific task
    """
    try:
        user = UserProfile.objects.get(user=User.objects.get(username=username))
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serilizer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def user_detail_pk(request, pk):
    """
    Get, udpate, or delete a specific task
    """
    try:
        user = UserProfile.objects.get(pk=pk)
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serilizer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



@api_view(['GET', 'PUT', 'DELETE'])
def authority_detail(request, pk):
    """
    Get, udpate, or delete a specific task
    """

    c=Authority.objects.filter(complaint=pk)
    


    if request.method == 'GET':
        serializer = AuthoritySerializer(c,many=True)
        return Response(serializer.data)


    # TODO : SEEEEEEEE HEEEEEEREEEEEEEEEEEe HOW TO PUT THIS

    elif request.method == 'PUT':
        serializer = AuthoritySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()            
            notification=Notifications(complaint=Complaints.objects.get(pk=request.data.get('complaint')),user=UserProfile.objects.get(pk=request.data.get('authority')),n_type=1,text="New Complaint to Resolve",date_time=datetime.datetime.now())
            notification.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','PUT'])
def complaint_comments(request, pk):
    """
    Get, udpate, or delete a specific task
    """

    
    try:
    	c=Comments.objects.filter(complaint=(Complaints.objects.get(pk=pk)))
    except:
    	c=None
    if request.method == 'GET':
        serializer = CommentSerializer(c,many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = CommentSerializerInput(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','DELETE'])
def complaint_comment_id(request, pk, pkc):
    """
    Get, udpate, or delete a specific task
    """

    
    try:
    	c=Comments.objects.filter(pk=pkc)
    except:
    	c=None
    if request.method == 'GET':
        serializer = CommentSerializer(c,many=True)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        c.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



@api_view(['GET'])
def notification_list(request, username):
    """
    List all tasks, or create a new task.
    """

    try:
        user = UserProfile.objects.get(user=User.objects.get(username=username))
    except user.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


    if request.method == 'GET':
        n = Notifications.objects.filter(user=user)
        serializer = NotificationSerializer(n, many=True)
        res=Response(serializer.data)
        n.update(status=True)
        return res


 
 

