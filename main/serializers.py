from django.contrib.auth.models import User, Group
from models import *
from rest_framework import serializers



class UserSerializer(serializers.ModelSerializer):
	first_name=serializers.CharField(source='user.first_name')
	last_name=serializers.CharField(source='user.last_name')
	email=serializers.CharField(source='user.email')
	username=serializers.CharField(source='user.username')
	user_id=serializers.IntegerField(source='user.pk')
	class Meta:
		model = UserProfile
		fields = ('user_id','first_name','last_name','email', 'username', 'contact_number', 'designation' ,'entry_number','contact_number','user_type')


class ComplaintSerializer(serializers.ModelSerializer):
	complaints_id=serializers.IntegerField(source='pk')
	class Meta:
		model=Complaints
		fields=('complaints_id','user','c_type','c_subtype','title','description','anonymous','visiblity_level','urgent','date_time_submit','date_time_resolve','no_of_upvotes','status','official_reply');



class ComplaintSerializerInput(serializers.ModelSerializer):
	class Meta:
		model=Complaints
		fields=('user','c_type','c_subtype','title','description','anonymous','visiblity_level','urgent','date_time_submit','date_time_resolve','no_of_upvotes','status','official_reply');

class AuthoritySerializer(serializers.ModelSerializer):
	class Meta:
		model=Authority
		fields=('complaint','authority');


class CommentSerializer(serializers.ModelSerializer):
	comment_id=serializers.IntegerField(source='pk')
	class Meta:
		model=Comments
		fields=('comment_id','complaint','user','text','date_time');


class CommentSerializerInput(serializers.ModelSerializer):
	class Meta:
		model=Comments
		fields=('complaint','user','text','date_time');


class NotificationSerializer(serializers.ModelSerializer):
	notification_id=serializers.IntegerField(source='pk')
	class Meta:
		model=Notifications
		fields=('notification_id','complaint','user','n_type','status','text','date_time','date_time_seen');